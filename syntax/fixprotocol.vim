if exists("b:current_syntax")
  finish
endif

syn case ignore

" First field in the line
syn region fixField start="\s\d" end="[=\n]"he=e-1 contains=fixValue
syn region fixField start="^8" end="[=\n]"he=e-1 contains=fixValue
" all other fields in the line
syn region fixField start="" end="[=\n]"he=e-1 contains=fixValue
" values
syn region fixValue start="="hs=s+1 end="[\n]" contained

" setting the colors
hi def link fixField KeyWord
hi def link fixValue String
hi def link header Comment
hi def link footer Comment

let b:current_syntax = "fixprotocol"
